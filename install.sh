#!/bin/bash
DIR="$(cd "$(dirname "$0")" && pwd)"
echo "Install Arctis 5, 7, PRO + DAC, PRO Wireless"
rsync -uv ${DIR}/paths/steelseries-arctis-input.conf /usr/share/pulseaudio/alsa-mixer/paths/
rsync -uv ${DIR}/paths/steelseries-arctis-output-mono.conf /usr/share/pulseaudio/alsa-mixer/paths/
rsync -uv ${DIR}/paths/steelseries-arctis-output-stereo.conf /usr/share/pulseaudio/alsa-mixer/paths/
rsync -uv ${DIR}/profiles/steelseries-arctis-usb-audio.conf.conf /usr/share/pulseaudio/alsa-mixer/profile-sets/
rsync -uv ${DIR}/udev/90-pulseaudio-steelseries-arctis.rules /lib/udev/rules.d/90-pulseaudio-steelseries-arctis.rules
