# arctis-pro-wireless-pulse

Contains config scrips for pulseaudio and udev to use the stereo sink of the Arctis 5, 7, Pro+DAC and Pro Wireless headset.

## Installation
### Use installation Script
The easiest way is to run the install script 'install.sh' with sudo/root rights on ubuntu 18.04.

### Manual installation on Ubuntu 18.04
#### Find vendor id and device id

``` lsusb | grep SteelSeries ```

The output looks like:
``` Bus 001 Device 007: ID [VendorID]:[DeviceID] SteelSeries ApS ```

#### profiles in pulseaudio
The profiles is used to map channels and paths together. So the profile in the profiles folder need to be copied to
``` /usr/share/pulseaudio/alsa-mixer/profile-sets ```

#### Paths for pulseaudio
The Steelseries Arctis Headsets 5, 7, Pro + DAC and PRO Wireless has one input device and two output devices at PCM 0 the Mono and PCM 1 Stereo.
So there are three files for input and output mono and stereo in the paths folder. You need to copy the files to 
``` /usr/share/pulseaudio/alsa-mixer/paths ```

#### udev Rule
The file 90-pulseaudio-steelseries-arctis.rules contains the vendor id for SteelSeries and device ids for Acrtis 5, 7, PRO + DAC and PRO Wireless. If our device id is not contained in this file so append it. Copy the file to ``` /lib/udev/rules.d/ ``` so udev can active the Arctis profiles if one of the devices in 90-pulseaudio-steelseries-arctis.rules is detected by vendor and device id.
